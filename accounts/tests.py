from django.test import TestCase, Client
from django.urls import resolve, reverse
from appointment.views import home, appointment
from appointment.models import Appointment
from .views import login_view, signup_view, logout_view
from django.contrib.auth.models import User


from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
import time

# Create your tests here.

class UnitTestStatusPage(TestCase):
    def test_apakah_ada_url_ada(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_kosong_menggunakan_fungsi_home(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_apakah_fungsi_home_bekerja_saat_mendapat_request_GET(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')
        self.assertIn("Welcome", content)

    def test_apakah_ada_url_appointment_ada(self):
        c = Client()
        response = c.get(reverse('appointment:appointments'))
        self.assertEqual(response.status_code, 302)

    def test_apakah_url_appointments_menggunakan_fungsi_appointment(self):
        found = resolve(reverse('appointment:appointments'))
        self.assertEqual(found.func, appointment)
    
    def test_apakah_fungsi_appointment_bekerja_saat_mendapat_request(self):
        c = Client()
        response = c.get(reverse('appointment:appointments'))
        content = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        


    #Test untuk app accounts
    def test_apakah_ada_url_login(self):
        c = Client()
        response = c.get(reverse('accounts:login'))
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_url_login_menggunakan_fungsi_login(self):
        found = resolve(reverse('accounts:login'))
        self.assertEqual(found.func, login_view)
    
    def test_apakah_fungsi_login_bekerja_saat_mendapat_request(self):
        c = Client()
        response = c.get(reverse('accounts:login'))
        content = response.content.decode('utf-8')
        self.assertTemplateUsed(response, 'login.html')
        self.assertIn("Log in", content)

    def test_apakah_ada_url_signup(self):
        c = Client()
        response = c.get(reverse('accounts:signup'))
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_url_signup_menggunakan_fungsi_signup(self):
        found = resolve(reverse('accounts:signup'))
        self.assertEqual(found.func, signup_view)

    def test_apakah_fungsi_signup_bekerja_saat_mendapat_request(self):
        c = Client()
        response = c.get(reverse('accounts:signup'))
        content = response.content.decode('utf-8')
        self.assertTemplateUsed(response, 'signup.html')
        self.assertIn("Sign up", content)

    def test_apakah_ada_url_logout(self):
        c = Client()
        response = c.get(reverse('accounts:signup'))
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_url_logout_menggunakan_fungsi_logout(self):
        found = resolve(reverse('accounts:logout'))
        self.assertEqual(found.func, logout_view)


#Functional test for login, logout, and signup
class FunctionalTestAccounts(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)
        user = User.objects.create_user('admin', '', 'admin')

    def tearDown(self):
        self.browser.close()

    def test_ada_title_website(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        # User melihat title website yang dibuka tertulis Meet Lika
        self.assertIn('Meet Lika', self.browser.title)
    
    def test_bisa_login(self):
        # User membuka browser dengan mengakses url login
        self.browser.get(self.live_server_url + '/accounts/login')

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password')

        #Memasukkan input ke form
        userField.send_keys('admin')
        passField.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()
                
        #Setelah login user akan melihat username mereka pada halaman website
        self.assertIn('admin', self.browser.page_source)

    def test_bisa_logout(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url + '/accounts/login')

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password')

        #Memasukkan input ke form
        userField.send_keys('admin')
        passField.send_keys('admin')
        
        button = self.browser.find_element_by_id('login-button')
        button.click()
        time.sleep(5)

        #User menekan tombol logout
        buttonOut = self.browser.find_element_by_id('logout-button')
        buttonOut.click()
        time.sleep(5)
        
        #username user tidak lagi ditampilkan pada halaman website
        self.assertNotIn('admin', self.browser.page_source)

    def test_bisa_signup(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url + '/accounts/login')

        signup_link = self.browser.find_element_by_id('signup-link')
        signup_link.click()

        userField = self.browser.find_element_by_name('username')
        passField = self.browser.find_element_by_name('password1')
        passField2 = self.browser.find_element_by_name('password2')

        #Memasukkan input ke form
        userField.send_keys('lika')
        passField.send_keys('test123helo')
        passField2.send_keys('test123helo')

        button = self.browser.find_element_by_id('signup-button')
        button.click()
        time.sleep(5)
        
        #Setelah signup user akan melihat username mereka pada halaman website
        self.assertIn('lika', self.browser.page_source)


