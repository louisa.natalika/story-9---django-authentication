from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your views here.
def signup_view(request):
    not_valid = False
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        not_valid = True
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('appointment:appointments')
    form = UserCreationForm()
    arguments = {
        'form' : form,
        'not_valid': not_valid
    }
    return render(request, 'signup.html', arguments)

def login_view(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            #log in the user
            user = form.get_user()
            login(request, user)
            return redirect('appointment:appointments')
    else:
        form = AuthenticationForm()
        arguments = {
            'form' : form,
        }
        return render(request, 'login.html', arguments)

def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect('appointment:home')
