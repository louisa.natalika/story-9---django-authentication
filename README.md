[![pipeline status](https://gitlab.com/louisa.natalika/story-9---django-authentication/badges/master/pipeline.svg)](https://gitlab.com/louisa.natalika/story-9---django-authentication/commits/master)
[![pipeline status](https://gitlab.com/louisa.natalika/story-9---django-authentication/badges/master/coverage.svg)](https://gitlab.com/louisa.natalika/story-9---django-authentication/commits/master)

# About
A website to make an appointment with me

## Heroku
[appointment-with-lika.herokuapp.com](appointment-with-lika.herokuapp.com)

