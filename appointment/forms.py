from django import forms
from .models import Appointment

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['day', 'date', 'time', 'activities', 'place']
        widgets = {
            'day' : forms.Select(attrs={
					'class': 'form-control'
					}),
            'date' : DateInput(attrs={
					'class': 'form-control'
					}),
            'time' : TimeInput(attrs={
					'class': 'form-control'
					}),
            'activities' : forms.TextInput(attrs={
					'class': 'form-control'
					}),
            'place' : forms.TextInput(attrs={
					'class': 'form-control'
					}),
        }