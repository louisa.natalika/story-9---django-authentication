from django.shortcuts import render
from .forms import AppointmentForm
from .models import Appointment
from .forms import AppointmentForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your views here.

def home(request):
    return render(request, 'home.html')

@login_required(login_url='/accounts/login')
def appointment(request):
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():
            app = form.save(commit=False)
            app.name = request.user
            app.save()
    form = AppointmentForm()
    appointment = Appointment.objects.all()
    arguments = {
        'form': form,
        'appointments': appointment,
    }
    return render(request, 'appointment.html', arguments)