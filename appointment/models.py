from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User


days = [
    ('Mon', 'Monday'),
    ('Tue', 'Tuesday'),
    ('Wed', 'Wednesday'),
    ('Thu', 'Thursday'),
    ('Fri', 'Friday'),
    ('Sat', 'Saturday'),
    ('Sun', 'Sunday'),
]


class Appointment(models.Model):
    day = models.CharField(max_length=20, choices=days)
    date = models.DateField()
    time = models.TimeField()
    name = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    activities = models.CharField(max_length=300)
    place = models.CharField(max_length=100)

    def __str__(self):
        return self.activities
