from django.urls import path
from .views import home, appointment

app_name = 'appointment'

urlpatterns = [
    path('', home, name='home'),
    path('appointments/', appointment, name='appointments'),
]